/**
 * Created by igor.olshevsky on 10/19/15.
 */
import global from './config'
import local from './config.local'

let merge = (o1, o2) => {
    Object.keys(o2).forEach((prop) => {
        var val = o2[prop];
        if (o1.hasOwnProperty(prop)) {
            if (typeof val == 'object') {
                if (val && val.constructor != Array) {
                    val = merge(o1[prop], val);
                }
            }
        }
        o1[prop] = val;
    });
    return o1;
};

export default merge (global, local);