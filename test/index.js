import connection from './../src/helpers/connection'
import knex from 'knex'
import mock, {getTracker} from 'mock-knex'

let tracker = getTracker();
mock.mock(connection.knex);
tracker.install();
let id = 0;
tracker.on('query', function checkResult(query) {
    query.response([++id]);
});

import './tests/AccountServiceTest'