/**
 * Created by lenovo on 11/18/15.
 */
export default {
    API: {
        Id: "94b986aa-4a8d-e511-80f1-3863bb3610e8",
        Name: "Test Test",
        CompanyName: "Test Company",
        Telephone1: "1234567890",
        Fax: null,
        EmailAddress1: "test@dispostable.com",
        PrimaryContact: null,
        Address1_Line1: "Test Address",
        Address1_City: "Test City",
        Address1_StateOrProvince: "Test State",
        Address1_PostalCode: "Test Zip Code",
        createdon: "11/17/2015 4:46:00 PM",
        modifiedon: "11/17/2015 4:50:10 PM"
    }
}