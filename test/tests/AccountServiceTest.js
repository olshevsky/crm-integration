import AccountService from './../../src/service/AccountService'
import {expect} from 'chai'
import config from './../config/index'
import UserService from './../../src/service/UserService'
import APIAccount from './../mocks/ApiAccount'

describe("AccountService", () => {
    let iu = new UserService();
    iu.data = APIAccount.API;

    it("Create new dentist (TEENK)", () => {
        return iu.create(APIAccount.API).then(() => {
            return true;
        })
    });

    it("Create Bridge for User (TEENK)", () => {
        return iu.createBridge().then(() => {
            return true;
        })
    });

    it("Create Account for User (TEENK)", () => {
        return iu.createAccount().then(() => {
            return true;
        })
    });

    it("Create Assignment to Account (TEENK)", () => {
        return iu.assignToAccount().then(() => {
            return true;
        })
    });

    it("Create Assignment for Lab (TEENK)", () => {
        return iu.assignToLab().then(() => {
            return true;
        })
    });

    it("Check User Updates (TEENK)", () => {
        return iu.completeAssignment().then(() => {
            return true;
        })
    });
});