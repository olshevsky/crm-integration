import IUser from './../../models/bridge/User'
import Account from './../../models/Account'
import AccountRelation from './../../models/AccountRelation'
import LabRelation from './../../models/LabRelation'
import User from './../../models/User'
import moment from 'moment'

export default class UserService {

    constructor(data) {
        this.user = false;
        this.data = data;
        this.bridge = false;
        this.account = false;
        this.accountRelation = false;

        this.create = this.create.bind(this);
        this.createBridge = this.createBridge.bind(this);
        this.createAccount = this.createAccount.bind(this);
        this.assignToAccount = this.assignToAccount.bind(this);
        this.assignToLab = this.assignToLab.bind(this);
        this.completeAssignment = this.completeAssignment.bind(this);
        this.save = this.save.bind(this);
    }

    save(data) {
        this.data = data;
        return this
            .create()
            .then(this.createBridge)
            .then(this.createAccount)
            .then(this.assignToAccount)
            .then(this.assignToLab)
            .then(this.completeAssignment)
            .then(_ => ({integration: this.bridge, user: this.user}))
    }

    create() {
        let {data} = this;
        data = data || {}
        let user = new User({
            phone: data.Telephone1,
            status: 4,
            lab_id: 1,
            firstName: data.Name,
            zipCode: data.Address1_PostalCode,
            city: data.Address1_City,
            address: data.Address1_Line1,
            state: data.Address1_StateOrProvince,
            company: data.CompanyName,
            email: data.EmailAddress1 || ''
        });

        return user.save().then(_ => {
            this.user = user.toJSON();
            return {user}
        });
    }

    createAccount() {
        let {user} = this;

        let account = new Account({
            balance: 0,
            status: 1,
            name: user.firstName || "Generated",
            owner_id: user.id
        });

        return account.save().then(_ => {
            this.account = account.toJSON();
            return account;
        })
    }

    assignToLab() {
        let {account, lab, user} = this;

        let relation = new LabRelation({
            user_id: user.id,
            type: 1,
            role: 10,
            account_id: account.id,
            status: 1,
            lab_id: 1
        });

        return relation.save().then(_ => {
            this.relation = relation.toJSON();
            return relation
        })
    }

    assignToAccount() {
        let {account, user} = this;
        let relation = new AccountRelation({
            account_id: account.id,
            user_id: user.id,
            role: 0,
            creator_id: user.id
        });

        return relation.save().then(_ => {
            this.accountRelation = relation.toJSON();
            return relation;
        })
    }

    completeAssignment() {
        let {account, relation, user} = this;
        return User.forge({id: user.id}).save({
            defaultAccount_id: account.id,
            defaultAssignment_id: relation.id
        }).then(_ => user)
    }

    createBridge() {
        let {data, user} = this;

        let bridge = new IUser({
            user_id: user.id,
            _id: data.Id,
            createdon: moment(new Date(data.createdon)).format(),
            modifiedon: moment(new Date(data.modifiedon)).format()
        });

        return bridge.save().then(_ => {
            this.bridge = bridge.toJSON()
            return bridge
        })
    }
}