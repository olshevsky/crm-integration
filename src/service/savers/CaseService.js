/**
 * Created by lenovo on 12/3/15.
 */
import IUser from './../../models/bridge/User'
import Account from './../../models/Account'
import AccountRelation from './../../models/AccountRelation'
import LabRelation from './../../models/LabRelation'
import User from './../../models/User'
import Patient from './../../models/Patient'
import Issue from './../../models/Issue'
import AccountService from './../AccountService'
import IIssue from './../../models/bridge/Issue'
import moment from 'moment'
import statuses from './../../helpers/statuses'

export default class CaseService {
    constructor() {
        this.savePatient = this.savePatient.bind(this)
        this.getUser = this.getUser.bind(this)
        this.saveCase = this.saveCase.bind(this)
    }

    save(caseorder) {
        return this.getUser(caseorder).then(this.savePatient).then(this.saveCase).then(this.saveIntegration).catch(_ => console.log("Reject", _))
    }

    getUser(caseorder) {
        console.log("getUser", caseorder.Id, caseorder.byf_customer)
        return caseorder.byf_customer ? AccountService.get(caseorder.byf_customer.Id).then((user) => {return {user, caseorder}}) : Promise.reject(false)
    }

    savePatient({user, caseorder}) {
        console.log("savePatient", caseorder.Id)
        let {integration} = user;
        let u = user.user;
        let patient = new Patient({
            name: caseorder.byf_patientfullname,
            age: caseorder.ark_age || 0,
            dentist_id: u.id
        });
        return patient.save().then(() => ({patient, caseorder, user}));
    }

    saveCase({user, patient ,caseorder}) {
        console.log("saveCase", caseorder.Id)
        let {integration} = user;
        let u = user.user;
        let issue = new Issue({
            patient_id: patient.id,
            restoration: 'fixed',
            status_id: statuses[caseorder.byf_casestatus],
            barcode: caseorder.byf_name,
            replaceTeethN: caseorder.byf_teethnumber,
            shade: caseorder.byf_shade_list,
            lab_id: 1,
            dentist_id: u.id,
            account_id: u.defaultAccount_id,
            systemStatus: 0,
            createdAt: moment(new Date(caseorder.createdon)).format(),
            repair: caseorder.byf_redo != "No" ? 'repair' : null,
            dueDate: moment(new Date(caseorder.ark_listedduedate || false)).format()
        });
        return issue.save().then(() => ({user, patient, caseorder, issue}));
    }

    saveIntegration({user, patient, caseorder, issue}) {
        console.log("saveIntegration", caseorder.Id)
        let integration = new IIssue({
            issue_id: issue.id,
            turnAround: caseorder.ark_turn_around_time,
            _id: caseorder.Id,
            createdon: moment(new Date(caseorder.createdon)).format(),
            modifiedon: moment(new Date(caseorder.modifiedon)).format()
        });

        return integration.save().then(() => ({user, patient, caseorder, issue, integration}))
    }
}