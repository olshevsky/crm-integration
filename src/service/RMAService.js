/**
 * Created by igor.olshevsky on 10/19/15.
 */
import moment from 'moment'
import config from './../config/index'
import AbstractRestService from './AbstractRestService'
import IUser from './../models/bridge/User'
import Account from './../models/Account'
import AccountRelation from './../models/AccountRelation'
import LabRelation from './../models/LabRelation'
import RMA from './../models/RMA'

class RMAService extends AbstractRestService {
    save(body, cb, promises = []) {
        body.map(_rma => {
            promises.push(new Promise((resolve, reject) => {
                RMA.where({_id: _rma.Id}).fetch().then((rma) => {
                    if(!rma || rma == null) {
                        this.store(_rma).then(resolve)
                    } else {
                        this._localUpdate(_rma).then(resolve)
                    }
                })
            }))
        });

        if(this.options.count <= body.length) {
            this.options.page += 1;
            this.list().then(body => this.save(body, cb, promises));
        } else {
            console.log("RMA finished")
            Promise.all(promises).then(cb);
        }
    }

    _localUpdate(_rma) {
        return RMA.forge({id: _rma.Id}).save({
            redo: _rma.ark_redo,
            redoRepairReason: _rma.byf_redorepairreason,
            cost: _rma.ark_drcost,
            user_id: user.id,
            rootCause: _rma.ark_rootcause1 ? _rma.ark_rootcause1.Name : "",
            rootCauseDescription: _rma.ark_rootcausedescription1 || '',
            createdon: moment(new Date(_rma.createdon)).format(),
            modifiedon: moment(new Date(_rma.modifiedon)).format()
        })
    }

    get(id) {
        return new Promise((resolve, reject) => {
            RMA.where({_id: id}).fetch().then((result) => {
                if(result && result != null) {
                    result = result.toJSON();
                    resolve({rma: result.rma})
                } else {
                    this.single(id).then((data) => {
                        this.store(data).then((result) => {
                            let {rma} = result;
                            resolve({rma})
                        })
                    })
                }
            })
        })
    }

    store(_rma) {
        return new Promise((resolve, reject) => {
            IUser.where({_id: _rma.byf_customer ? _rma.byf_customer.Id : ''}).fetch().then((user) => {
                if(user && user != null) {
                    user = user.toJSON();
                    RMA.where({_id: _rma.byf_rmaid}).fetch().then((__r) => {
                        if(!__r || __r == null) {
                            let rma = new RMA({
                                redo: _rma.ark_redo,
                                redoRepairReason: _rma.byf_redorepairreason,
                                _id: _rma.byf_rmaid,
                                cost: _rma.ark_drcost,
                                user_id: user.user_id,
                                rootCause: _rma.ark_rootcause1 ? _rma.ark_rootcause1.Name : "",
                                rootCauseDescription: _rma.ark_rootcausedescription1 || '',
                                createdon: moment(new Date(_rma.createdon)).format(),
                                modifiedon: moment(new Date(_rma.modifiedon)).format()
                            });

                            rma.save().then((_u) => {
                                resolve({rma})
                            })
                        }
                    })
                }
            });
        })
    }

    load(cb = () => {}) {
        this.list().then(body => this.save(body, cb))
    }
}

export default new RMAService(`${config.api.endpoint}/rmas/`);