/**
 * Created by igor.olshevsky on 10/19/15.
 */
import moment from 'moment'
import config from './../config/index'
import AbstractRestService from './AbstractRestService'
import UserService from './savers/UserService'
import IUser from './../models/bridge/User'
import Account from './../models/Account'
import AccountRelation from './../models/AccountRelation'
import LabRelation from './../models/LabRelation'
import User from './../models/User'

let inArray = (needle, haystack = []) => {
    return haystack.indexOf(needle) !== -1
};

let allowed = config.users.map(uc => uc.value);

class AccountService extends AbstractRestService {
    save(body, cb, promises = []) {
        body.map(account => {
            if(config.crm.all) {
                promises.push(
                    new Promise((resolve, reject) => {
                        IUser.where({_id: account.Id}).fetch().then((user) => {
                            if(!user || user == null) {
                                this.store(account).then(resolve)
                            } else {
                                this._localUpdate(user.user.id, account).then(resolve)
                            }
                        })
                    })
                )
            } else {
                if(inArray(account.Id, allowed) || inArray(account.Name, allowed)) {
                    promises.push(
                        new Promise((resolve, reject) => {
                            IUser.where({_id: account.Id}).fetch().then((user) => {
                                if(!user || user == null) {
                                    this.store(account).then(resolve)
                                } else {
                                    user = user.toJSON();
                                    this._localUpdate(user.user_id, account).then(resolve)
                                }
                            })
                        })
                    )
                }
            }
        });

        if(this.options.count <= body.length) {
            this.options.page += 1;
            this.list().then(body => this.save(body, cb, promises));
        } else {
            Promise.all(promises).then(cb);
        }
    }

    _localUpdate(id, _user) {
        return User.forge({id: id}).save({
            phone: _user.Telephone1,
            firstName: _user.Name,
            zipCode: _user.Address1_PostalCode,
            city: _user.Address1_City,
            address: _user.Address1_Line1,
            state: _user.Address1_StateOrProvince,
            company: _user.CompanyName,
            email: _user.EmailAddress1 || ''
        }).catch(() => {})
    }

    get(id) {
        return new Promise((resolve, reject) => {
            IUser.where({_id: id}).fetch({withRelated: ['user']}).then((result) => {
                if(result && result != null) {
                    result = result.toJSON();
                    resolve({user: result.user, integration: result})
                } else {
                    this.single(id).then((data) => {
                        if(config.crm.all) {
                            this.single(id).then((data) => {
                                this.store(data).then((result) => {
                                    let {user, integration} = result;
                                    resolve({user, integration})
                                })
                            })
                        } else {
                            if(inArray(data.Id, allowed) || inArray(data.Name, allowed)) {
                                this.single(id).then((data) => {
                                    this.store(data).then((result) => {
                                        let {user, integration} = result;
                                        resolve({user, integration})
                                    })
                                })
                            } else {
                                reject()
                            }
                        }
                    })
                }
            })
        })
    }

    store(data) {
        let iu = new UserService();
        return iu.save(data)
    }

    load(cb = () => {}) {
        IUser.fetchAll().then(result => {
            result = result.toJSON();
            result.map(_rr => {
                allowed.push(_rr._id)
            });

            this.list().then((body) => {
                this.save(body, cb)
            })
        });
    }
}

export default new AccountService(`${config.api.endpoint}/accounts/`);