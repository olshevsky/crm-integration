/**
 * Created by igor.olshevsky on 10/19/15.
 */
import moment from 'moment'
import config from './../config/index'
import AbstractRestService from './AbstractRestService'
import AccountService from './AccountService'

import IIssue from './../models/bridge/Issue'
import Issue from './../models/Issue'
import Patient from './../models/Patient'
import User from './../models/User'
import statuses from './../helpers/statuses'
import CaseSavingService from './savers/CaseService'
import IUser from './../models/bridge/User'

class CaseService extends AbstractRestService {
    save(body, cb, promises = []) {
        body.map(_case => {
            promises.push(
                new Promise((resolve, reject) => {
                    IIssue.where({_id: _case.Id}).fetch({withRelated: ['issue']}).then((_IIssue) => {
                        console.log("Start saving", _case.Id)
                        if(!_IIssue || _IIssue == null) {
                            this.store(_case).then(resolve);
                        }
                        else {
                            console.log("Try To SYNC");
                            this.update(_IIssue, _case).then(resolve)
                        }
                    })
                })
            )
        });

        if(this.options.count <= body.length) {
            this.options.page += 1;
            this.list().then(body => this.save(body, cb, promises));
        } else {
            console.log("CaseService finished");
            Promise.all(promises).then(cb);
        }
    }

    get(id) {
        return new Promise((resolve, reject) => {
            IIssue.where({_id: id}).fetch({withRelated: ['issue']}).then((result) => {
                if(result && result != null) {
                    result = result.toJSON();
                    resolve({
                        issue: result.issue,
                        integration: result
                    })
                } else {
                    if(config.crm.all) {
                        this.single(id).then((data) => {
                            this.store(data).then((result) => {
                                resolve(result)
                            })
                        })
                    } else {
                        reject()
                    }
                }
            })
        })
    }

    update(issue, caseorder) {
        console.log("Update...");
        return Issue.forge({id: issue.issue.id}).save({
            restoration: 'fixed',
            status_id: statuses[caseorder.byf_casestatus],
            barcode: caseorder.byf_name,
            replaceTeethN: caseorder.byf_teethnumber,
            shade: caseorder.byf_shade_list,
            lab_id: 1,
            systemStatus: 0,
            createdAt: moment(new Date(caseorder.createdon)).format(),
            repair: caseorder.byf_redo != "No" ? 'repair' : null,
            dueDate: moment(new Date(caseorder.ark_listedduedate || false)).format()
        })
    }

    store(caseorder) {
        let is = new CaseSavingService();
        return is.save(caseorder)
    }

    load(cb = () => {}) {
        this.list().then((body) => {
            this.save(body, cb)
        })
    }
}

export default new CaseService(`${config.api.endpoint}/caseorders/`);