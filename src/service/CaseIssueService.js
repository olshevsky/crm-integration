/**
 * Created by igor.olshevsky on 10/19/15.
 */
import moment from 'moment'
import config from './../config/index'
import AbstractRestService from './AbstractRestService'
import CaseService from './CaseService'

import CaseIssues from './../models/CaseIssues'

class CaseIssueService extends AbstractRestService {
    save(body, cb, promises = []) {
        body.map(_case => {
            promises.push(
                new Promise((resolve, reject) => {
                    CaseIssues.where({_id: _case.Id}).fetch().then((_IIssue) => {
                        if(!_IIssue || _IIssue == null) {
                            this.store(_case).then(resolve);
                        } else {
                            this._localUpdate(_case).then(resolve);
                        }
                    })
                })
            )
        });

        if(this.options.count <= body.length) {
            this.options.page += 1;
            this.list().then(body => this.save(body, cb));
        } else {
            console.log("CaseIssueService finished")
            Promise.all(promises).then(cb)
        }
    }

    _localUpdate(caseissue) {
        return CaseIssues.forge({id: caseissue.Id}).save({
            _id: caseissue.byf_caseissueid || '',
            createdon: moment(new Date(caseissue.createdon)).format(),
            modifiedon: moment(new Date(caseissue.modifiedon)).format(),
            text: caseissue.ark_category1 && caseissue.ark_category1 != null ?
                caseissue.ark_category1.Name : ''
        })
    }

    get(id) {
        return new Promise((resolve, reject) => {
            CaseIssues.where({_id: id}).fetch({withRelated: ['issue']}).then((result) => {
                if(result && result != null) {
                    result = result.toJSON();
                    resolve(result.issue, result)
                } else {
                    this.single(id).then((data) => {
                        this.store(data).then((issue) => {
                            resolve(issue)
                        })
                    })
                }
            })
        })
    }

    store(caseissue) {
        return new Promise((resolve, reject) => {
            if(caseissue.byf_caseorder && caseissue.byf_caseorder != null) {
                CaseService.get(caseissue.byf_caseorder.Id).then((result) => {
                    let {issue, integration} = result;
                    let caseIssue = new CaseIssues({
                        _id: caseissue.byf_caseissueid || '',
                        issue_id: issue.id,
                        responseTime: issue.byf_response_time,
                        user_id: issue.dentist_id,
                        cost: caseissue.ark_drcost,
                        createdon: moment(new Date(caseissue.createdon)).format(),
                        modifiedon: moment(new Date(caseissue.modifiedon)).format(),
                        text: caseissue.ark_category1 && caseissue.ark_category1 != null ?
                            caseissue.ark_category1.Name : ''
                    });
                    caseIssue.save().then(() => {
                        resolve(caseIssue);
                    });
                }).catch(() => {
                    console.log(`${caseissue.byf_caseissueid} does not saved. Check you configuration.`)
                })
            } else {

            }
        })
    }

    load(cb) {
        this.list().then((body) => {
            this.save(body, cb)
        })
    }
}

export default new CaseIssueService(`${config.api.endpoint}/caseissues/`);