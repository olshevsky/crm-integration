/**
 * Created by igor.olshevsky on 10/19/15.
 */
import moment from 'moment'
import config from './../config/index'
import AbstractRestService from './AbstractRestService'
import IUser from './../models/bridge/User'
import IIssue from './../models/bridge/Issue'
import CaseService from './CaseService'
import Invoices from './../models/Invoices'

class CaseInvoicesService extends AbstractRestService {
    save(body, cb, promises = []) {
        body.map(_invoice => {
            promises.push(
                new Promise((resolve, reject) => {
                    Invoices.where({_id: _invoice.Id}).fetch().then((invoice) => {
                        if(!invoice || invoice == null) {
                            this.store(_invoice).then(resolve)
                        } else {
                            this._localUpdate(_invoice).then(resolve);
                        }}
                    );
                })
            )
        });

        if(this.options.count <= body.length) {
            this.options.page += 1;
            this.list().then(body => this.save(body, cb, promises));
        } else {
            console.log("CaseInvoicesService finished");
            Promise.all(promises).then(cb);
        }
    }

    _localUpdate(_order) {
        return Invoices.forge({id: _order.Id}).save({
            name: _order.byfname || '',
            date: moment(new Date(_order.byf_invoicedate)).format(),
            createdon: moment(new Date(_order.createdon)).format(),
            modifiedon: moment(new Date(_order.modifiedon)).format(),
            totalAmount: _order.byf_totalamount,
            taxableTotal: _order.ark_taxable_total,
            salesTaxAmount: _order.byf_salestaxamount,
            totalDiscountAmount: _order.byf_total_discount_amount,
            finalAmount: _order.byf_finalamount
        })
    }

    get(id) {
        return new Promise((resolve, reject) => {
            Invoices.where({_id: id}).fetch({withRelated: ['issue']}).then((result) => {
                if(result && result != null) {
                    result = result.toJSON();
                    resolve(result)
                } else {
                    if(config.crm.all) {
                        this.single(id).then((data) => {
                            return this.store(data).then((result) => {
                                resolve(result)
                            })
                        })
                    } else {
                        reject()
                    }
                }
            })
        })
    }

    store(invoice) {
        return new Promise((resolve, reject) => {
            Promise.all([
                IIssue.where(
                    {
                        _id: invoice.byf_caseorder && invoice.byf_caseorder != null ?
                            invoice.byf_caseorder.Id : null}
                ).fetch({withRelated: ['issue']}),
            ]).then((results) => {
                let __case = results.pop();
                __case = __case.toJSON();
                Invoices.where(
                    {
                        _id: invoice.byf_caseinvoiceid
                    }).fetch().then((__invoice) => {
                    if(__case && __case != null) {
                        if(!__invoice || __invoice == null) {
                            let _invoice = new Invoices({
                                _id: invoice.byf_caseinvoiceid,
                                name: invoice.byfname || '',
                                date: moment(new Date(invoice.byf_invoicedate)).format(),
                                issue_id: __case.issue.id,
                                createdon: moment(new Date(invoice.createdon)).format(),
                                patient: invoice.byf_patientfullname,
                                teethNumber: invoice.byf_teethnumber,
                                shadeList: invoice.byf_shadelist,
                                drName: invoice.ark_drname1,
                                modifiedon: moment(new Date(invoice.modifiedon)).format(),
                                user_id: __case.issue.dentist_id,
                                totalAmount: invoice.byf_totalamount,
                                taxableTotal: invoice.ark_taxable_total,
                                salesTaxAmount: invoice.byf_salestaxamount,
                                totalDiscountAmount: invoice.byf_total_discount_amount,
                                finalAmount: invoice.byf_finalamount
                            });

                            _invoice.save().then(() => {
                                console.log(_invoice.id)
                                resolve(_invoice);
                            })
                        }
                    }
                })
            })
        });
    }

    load(cb = () => {}) {
        return this.list().then((body) => {
            this.save(body, cb)
        })
    }
}

export default new CaseInvoicesService(`${config.api.endpoint}/caseinvoices/`);