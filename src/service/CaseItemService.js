/**
 * Created by igor.olshevsky on 10/19/15.
 */
import moment from 'moment'
import config from './../config/index'
import AbstractRestService from './AbstractRestService'
import IUser from './../models/bridge/User'
import IIssue from './../models/bridge/Issue'
import InvoiceItem from './../models/InvoiceItem'
import InvoiceService from './CaseInvoicesService'
import Invoices from './../models/Invoices'

class CaseItemService extends AbstractRestService {
    save(body, cb, promises = []) {
        return new Promise((resolve, reject) => {
            body.map(_invoice => {
                promises.push(new Promise((resolve, reject) => {
                    InvoiceItem.where({_id: _invoice.Id}).fetch().then((invoice) => {
                        if(!invoice || invoice == null) {
                            this.store(_invoice).then(resolve)
                        } else {
                            this._localUpdate(_invoice).then(resolve);
                        }
                    });
                }))
            });

            if(this.options.count <= body.length) {
                this.options.page += 1;
                this.list().then(body => this.save(body, cb, promises));
            } else {
                resolve(true);
                console.log("CaseItemService finished")
                Promise.all(promises).then(cb);
            }
        });
    }

    _localUpdate(item) {
        return InvoiceItem.forge({id: item.Id}).save({
            _id: item.byf_caseinvoiceitemid,
            quantity: item.byf_quantity,
            amount: item.byf_amount,
            productCode: item.byf_product.Name || '',
            productId: item.byf_product.Id || '',
            extendedAmount: item.byf_extendedamount,
            createdon: moment(new Date(item.createdon)).format(),
            modifiedon: moment(new Date(item.modifiedon)).format()
        })
    }

    get(id) {
        return new Promise((resolve, reject) => {
            InvoiceItem.where({_id: id}).fetch({withRelated: ['invoice']}).then((result) => {
                if(result && result != null) {
                    result = result.toJSON()
                    resolve(result.issue, result)
                } else {
                    this.single(id).then((data) => {
                        this.store(data).then((issue) => {
                            resolve(issue)
                        })
                    })
                }
            })
        })
    }

    store(item) {
        return new Promise((resolve, reject) => {
            InvoiceService.get(item.byf_caseinvoice.byf_caseinvoiceid).then((invoice) => {
                let invoiceItem = new InvoiceItem({
                    _id: item.byf_caseinvoiceitemid,
                    quantity: item.byf_quantity,
                    amount: item.byf_amount,
                    productCode: item.byf_product.Name || '',
                    productId: item.byf_product.Id || '',
                    extendedAmount: item.byf_extendedamount,
                    invoice_id: invoice.id,
                    createdon: moment(new Date(item.createdon)).format(),
                    modifiedon: moment(new Date(item.modifiedon)).format()}
                );

                invoiceItem.save().then(() => {
                    resolve(invoiceItem)
                })
            }).catch(() => {
                console.log(`${item.byf_caseinvoiceitemid} does not saved. Check your configuration.`)
            })
        });
    }

    load(cb) {
        return this.list().then((body) => {
            this.save(body, cb)
        })
    }
}

export default new CaseItemService(`${config.api.endpoint}/caseinvoiceitems/`);