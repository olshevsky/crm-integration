/**
 * Created by igor.olshevsky on 10/19/15.
 */
import superagent from 'superagent'
import config from './../config/index'
import url from 'url'

class AbstractRestService {
    constructor(url) {
        this.url = url;

        this.options = {
            page: 1,
            count: 200,
            order: 'createdon'
        };

        this.credentials = config.crm.credentials
        this.request = superagent;

    }

    static inject(request, credentials) {
        return new Promise((resolve, reject) => {
            request
                .set('X-ApiUsername', credentials.username)
                .set('x-ApiPassword', credentials.password)
                .set('Content-Type', "application/json")
                .then((response) => {
                    resolve(response.body);
                }, (err) => {
                    reject(err)
                })
        })
    }

    list() {
        return AbstractRestService.inject(
            this.request
                .get(`${this.url}/${this.options.page + ""}/${this.options.count + ""}/${this.options.order + ""}`),
            this.credentials
        )
    }

    single(id) {
        return AbstractRestService.inject(
            this.request
                .get(`${this.url}/${id}/`),
            this.credentials
        )
    }

    create(data) {
        return AbstractRestService.inject(
            this.request
                .post(`${this.url}/`)
                .send(JSON.stringify(data)),
            this.credentials
        )
    }

    update(id, data) {
        return AbstractRestService.inject(
            this.request
                .put(`${this.url}${id}/`)
                .send(JSON.stringify(data)),
            this.credentials
        )
    }

    delete() {
        return this.request
            .del(`${this.url}/${id}/`)
            .set('X-ApiUsername', this.credentials.username)
            .set('x-ApiPassword', this.credentials.password)
    }
}

export default AbstractRestService