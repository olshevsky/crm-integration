/**
 * Created by igor.olshevsky on 10/19/15.
 */
export default {
    api: {
        endpoint: 'http://52.89.207.54/api'
    },
    db: {
        client: 'mysql',
        connection: {
            host     : 'localhost',
            user     : '',
            password : '',
            database : '',
            charset  : 'utf8'
        }
    },
    system: {
        socket: "http://localhost:3000"
    },
    crm: {
        all: false,
        credentials: {
            username: "byfdeveloper",
            password: "ByFD3v3l0per$2015"
        }
    },

    users: [
        {
            key: "name",
            value: "Soriano, George A., DMD"
        }
    ]
}