import config from './../config/index'

import knex from 'knex'
import bookshelf from 'bookshelf'

export default bookshelf(knex(config.db))