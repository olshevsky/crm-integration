/**
 * Created by lenovo on 11/27/15.
 */
export default {
    "Action Required": 1,
    "In Progress": 2,
    "Completed": 3,
    "Shipped": 4,
    "Pending": 5,
    "Canceled": 6
}

export var statusesMap = {
    1: "Action Required",
    2: "In Progress",
    3: "Completed",
    4: "Shipped",
    5: "Pending",
    6: "Canceled"
};