/**
 * Created by igor.olshevsky on 10/19/15.
*/
import config from './config/index'
import Client from 'socket.io-client'
import AccountService from './service/AccountService'
import CaseIssueService from './service/CaseIssueService'
import CaseInvoicesService from './service/CaseInvoicesService'
import CaseService from './service/CaseService'
import CaseItemService from './service/CaseItemService'
import RMAService from './service/RMAService'
import User from './models/User'
import Issue from './models/Issue'
import IUser from './models/bridge/User'
import IIssue from './models/bridge/Issue'
import Agenda from 'agenda'
import statuses, {statusesMap} from './helpers/statuses'
import moment from 'moment'
process.argv[2] = process.argv[2] || false;
switch(process.argv[2]) {
    case '--accounts' : {
        AccountService.load();
        break;
    }
    case '--invoices' : {
        CaseInvoicesService.load();
        break;
    }
    case '--cases': {
        CaseService.load();
        break;
    }
    case '--issues' : {
        CaseIssueService.load();
        break;
    }
    case '--items' : {
        CaseItemService.load();
        break;
    }
    case '--rmas' : {
        RMAService.load();
        break;
    }
    case '--all' : {
        AccountService.load(() => {
            CaseService.load(() => {
                CaseIssueService.load();
                CaseInvoicesService.load(() => {
                    CaseItemService.load();
                    RMAService.load();
                })
            });
        });
    }
}

setInterval(() => {
    console.log("Run...");
    AccountService.load(() => {
        CaseService.load(() => {
            CaseIssueService.load();
        });
        CaseInvoicesService.load(() => {
            CaseItemService.load();
        });
        RMAService.load();
    });
}, 3600000);

//
//
//CaseIssueService.load();
//CaseInvoicesService.load();
//CaseItemService.load();
//RMAService.load();

let io = Client(config.system.socket);

io.on('connect', () => {
    console.log("Connected");
});

io.on("event", (data) => {
    data.events.map((event) => {
        switch(event.type) {
            case 1 : {
                // New
                let {issue} = event.metadata;
                IUser.where({user_id: issue.dentist.id}).fetch({withRelated: ['user']}).then(u => {
                    u = u.toJSON();
                    CaseService.create({
                        byf_customer: {
                            Id: u._id
                        },
                        byf_casestatus: statusesMap[issue.status.id || issue.status],
                        ark_listedduedate: moment(new Date(issue.dueDate)).format(),
                        byf_patientfullname: issue.patient.name,
                        byf_name: issue.barcode,
                        byf_redo: issue.repair
                    }).then(data => {
                        let ii = new IIssue({
                            issue_id: issue.id,
                            _id: data.Id,
                            createdon: moment(new Date(data.createdon)).format(),
                            modifiedon: moment(new Date(data.modifiedon)).format()
                        });

                        ii.save()
                    });
                });
                break;
            }
            case 2: {
                let {issue} = event.metadata;
                IUser.where({user_id: issue.dentist.id}).fetch({withRelated: ['user']}).then(u => {
                    if(u && u != null) {
                        IIssue.where({issue_id: issue.id}).fetch().then((iss) => {
                            if(iss && iss != null) {
                                iss = iss.toJSON();
                                u = u.toJSON();
                                CaseService.update(iss._id, {
                                    byf_customer: {
                                        Id: u._id
                                    },
                                    byf_name: issue.barcode,
                                    byf_casestatus: statusesMap[issue.status],
                                    ark_listedduedate: moment(new Date(issue.dueDate)).format(),
                                    byf_patientfullname: issue.patient.name,
                                    byf_redo: issue.repair
                                }).then(data => {

                                })
                            } else {
                                Object.keys(statuses).map(k => statuses[k] == (issue.status.id || issue.status) ? k : "Pending").pop()
                                CaseService.create({
                                    byf_customer: {
                                        Id: u._id
                                    },
                                    byf_name: issue.barcode,
                                    byf_casestatus: statusesMap[issue.status],
                                    ark_listedduedate: moment(new Date(issue.dueDate)).format(),
                                    byf_patientfullname: issue.patient.name,
                                    byf_redo: issue.repair
                                }).then(data => {
                                    let ii = new IIssue({
                                        issue_id: issue.id,
                                        _id: data.Id,
                                        createdon: moment(new Date(data.createdon)).format(),
                                        modifiedon: moment(new Date(data.modifiedon)).format()
                                    });

                                    ii.save()
                                });
                            }
                        })
                    }
                });
                break;
            }
            case 6: {
                let {issue} = event.metadata;
                IUser.where({user_id: issue.dentist.id}).fetch({withRelated: ['user']}).then(u => {
                    if(u && u != null) {
                        IIssue.where({issue_id: issue.id}).fetch().then((iss) => {
                            if(iss && iss != null) {
                                iss = iss.toJSON();
                                u = u.toJSON();
                                CaseService.update(iss._id, {
                                    byf_customer: {
                                        Id: u._id
                                    },
                                    byf_name: issue.barcode,
                                    byf_casestatus: statusesMap[issue.status],
                                    ark_listedduedate: moment(new Date(issue.dueDate)).format(),
                                    byf_patientfullname: issue.patient.name,
                                    byf_redo: issue.repair
                                }).then(data => {

                                })
                            } else {
                                Object.keys(statuses).map(k => statuses[k] == (issue.status.id || issue.status) ? k : "Pending").pop()
                                CaseService.create({
                                    byf_customer: {
                                        Id: u._id
                                    },
                                    byf_casestatus: statusesMap[issue.status],
                                    ark_listedduedate: moment(new Date(issue.dueDate)).format(),
                                    byf_patientfullname: issue.patient.name,
                                    byf_redo: issue.repair
                                }).then(data => {
                                    let ii = new IIssue({
                                        issue_id: issue.id,
                                        _id: data.Id,
                                        createdon: moment(new Date(data.createdon)).format(),
                                        modifiedon: moment(new Date(data.modifiedon)).format()
                                    });

                                    ii.save()
                                });
                            }
                        })
                    }
                });
                break;
            }
            case 20 : {
                // Registration
                let {user} = event.metadata;
                console.log(user);
                AccountService.create({
                    Name: user.signature,
                    CompanyName: user.company,
                    Telephone1: parseInt(user.phone.replace(/[^0-9]/, '')),
                    EmailAddress1: user.email,
                    Address1_Line1: user.address,
                    Address1_City: user.city,
                    Address1_StateOrProvince: user.state,
                    Address1_PostalCode: user.zipCode,
                }).then((data) => {
                    let uIntegration = new IUser({
                        user_id: user.id,
                        _id: data.Id,
                        createdon: moment(new Date(data.createdon)).format(),
                        modifiedon: moment(new Date(data.modifiedon)).format()
                    });

                    uIntegration.save()
                });
                break;
            }
            case 21 : {
                // Profile updated
                let {user} = event.metadata;
                IUser.where({user_id: user.id}).fetch({withRelated: ['user']}).then(u => {
                    if(u && u != null) {
                        u = u.toJSON();
                        AccountService.update(u._id, {
                            Name: user.signature,
                            CompanyName: user.company,
                            Telephone1: parseInt(user.phone.replace(/[^0-9]/, '')),
                            EmailAddress1: user.email,
                            Address1_Line1: user.address,
                            Address1_City: user.city,
                            Address1_StateOrProvince: user.state,
                            Address1_PostalCode: user.zipCode,
                        }).then(data => {
                            console.log(data);
                        })
                    } else {
                        AccountService.create({
                            Name: user.signature,
                            CompanyName: user.company,
                            Telephone1: user.name,
                            EmailAddress1: user.email,
                            Address1_Line1: user.address,
                            Address1_City: user.city,
                            Address1_StateOrProvince: user.state,
                            Address1_PostalCode: user.zipCode,
                        }).then((data) => {
                            console.log(data);

                            let uIntegration = new IUser({
                                user_id: user.id,
                                _id: data.Id,
                                createdon: moment(new Date(data.createdon)).format(),
                                modifiedon: moment(new Date(data.modifiedon)).format()
                            });

                            uIntegration.save()
                        });
                    }
                });
                break;
            }
        }
    });
});
