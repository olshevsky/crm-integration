/**
 * Created by igor.olshevsky on 10/19/15.
 */
import connection from './../helpers/connection'
import User from './User'
export default connection.Model.extend({
    tableName: "cases",
    dentist: function() {
        return this.belongsTo(User, 'dentist_id')
    }
});