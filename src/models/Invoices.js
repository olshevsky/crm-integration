/**
 * Created by igor.olshevsky on 10/19/15.
 */
import connection from './../helpers/connection'

import User from './User'
import Issue from './Issue'

export default connection.Model.extend({
    tableName: "__case_invoices",
    user: function() {
        return this.belongsTo(User, 'user_id')
    },
    issue: function() {
        return this.belongsTo(Issue, 'issue_id')
    }
});