/**
 * Created by igor.olshevsky on 10/19/15.
 */
import connection from './../../helpers/connection'
import BIssue from './../Issue'

let Issue = connection.Model.extend({
    tableName: "__integration_cases",
    issue() {
        return this.belongsTo(BIssue, 'issue_id')
    }
});

export default Issue;