/**
 * Created by igor.olshevsky on 10/19/15.
 */
import connection from './../../helpers/connection'
import BUser from './../User'

let User = connection.Model.extend({
    tableName: "__integration_users",
    user() {
        return this.belongsTo(BUser, 'user_id')
    }
});

export default User