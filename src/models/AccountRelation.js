import connection from './../helpers/connection'

export default connection.Model.extend({
    tableName: "user_account"
});