/**
 * Created by igor.olshevsky on 10/19/15.
 */
import connection from './../helpers/connection'
import BIssue from './Issue'
export default connection.Model.extend({
    tableName: "__case_issues",
    issue() {
        return this.belongsTo(BIssue, 'issue_id')
    }
});