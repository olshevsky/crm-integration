/**
 * Created by igor.olshevsky on 10/19/15.
 */
import connection from './../helpers/connection'

import Invoices from './Invoices'

export default connection.Model.extend({
    tableName: "__invoice_items",
    invoice: function() {
        return this.belongsTo(Invoices, 'invoice_id')
    }
});