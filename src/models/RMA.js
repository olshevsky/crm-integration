/**
 * Created by igor.olshevsky on 10/19/15.
 */
import connection from './../helpers/connection'

export default connection.Model.extend({
    tableName: "__rmas"
});